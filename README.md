# README #

Instalador do ambiente de webservice das serventias para visualização de matrícula (VM) e Banco de dados light (BDLight).

## Como instalar? ##

No servidor da serventia disponibilizado, apenas rode o seguinte comando (na pasta **home** do usuário **onr**):

**`onr@server-name:~$`** `curl --insecure https://bitbucket.org/tpix/onr-ws-installer/raw/master/ws.onr.installer.sh | bash`

Quando pedir senha, usar a mesma senha que utilizou para conectar no servidor.

Durante o processo de instalação, será criada a pasta `~/public_html` com código fonte do sistema.

Após a instalação, você deverá colocar nessa pasta o json de configuração do google para o cartório que será enviado pelo Sérgio e editar o arquivo `~/public_html/.env`
```
	# Config do GOOGLE
	GOOGLE_CONFIG=
	GOOGLE_NAMESPACE=bdlight
	GOOGLE_BUCKET=

	# Config do SISTEMA
	PADRAO_IMAGENS=
	DENOMINACAO=
	CNS=
	PRIV_KEY=
	WS_TOKEN=http://ws.arisp.com.br/token.asmx?wsdl
	IGNORE_HASH=false
	
	# Config do PARSE
	PARSE_APP_ID=
	PARSE_MASTER_KEY=
	PARSE_URL=
	PARSE_PORT=1337
	PARSE_ENDPOINT=parse
```

## Dicionário do `.env` ##

Segue uma leve explicação sobre esse arquivo. Diferente do funcionamento do CodeIgniter 4 e do Laravel, onde o `.env` já está incorporado no framework, este sistema foi feito baseado no CodeIgniter 3 para rodar inicialmente no PHP 5.6 utilizando como base o código do 5risp, então eu fiz uma leve modificação no framework para funcionar no PHP7.4 (uma necessidade que surgiu na época) e aproveitei e implementei esse `.env`, que é lido no arquivo `application/config/constants.php` e transformado cada valor numa constante.

Outras constantes podem ser declaradas nesse arquivo.

### **Config do GOOGLE**

|Constante            |Descrição                                   | 
|---------------------|--------------------------------------------|
|**GOOGLE_CONFIG**    | Arquivo de configuração do cliente que será enviado pelo Sérgio. Colocar na raíz do sistema. Pode ser incluído numa pasta dentro do sistema, basta colocar o nome da pasta na frente. Ele irá considerar a raiz do sistema sempre como pasta inicial. Não é necessário colocar `/` na frente.|
|**GOOGLE_NAMESPACE** | Namespace onde ficará o bdlight do cliente. Geralmente é bdlight, não sei se é padrão.|
|**GOOGLE_BUCKET**    | Bucket onde ficam as imagens das matrículas. Sérgio envia junto com os dados do servidor.|

### **Config do SISTEMA**

|Constante            |Descrição                                   | 
|---------------------|--------------------------------------------|
|**PADRAO_IMAGENS**   | Cada serventia utiliza um padrão numérico de imagens diferente. O Sérgio informará o padrão a ser utilizado. Não foram testados ainda todos os padrões, foram testados apenas os seguintes padrões: `1, 5`|
|**DENOMINACAO**      | Nome da serventia. Não está em uso.|
|**CNS**              | CNS da serventia. Não está em uso.|
|**PRIV_KEY**         | Chave privada utilizada para geração do hash. Após a instalação do sistema, Informar ao Sérgio as URLs e ele pedirá para o Roberto gerar a chave.|
|**WS_TOKEN**         | Webservice de validação do Token. Por default, está apontando para produção. Caso precise alterar, a URL de homologação é [http://hml.ws.onr.org.br/token.asmx?wsdl](http://hml.ws.onr.org.br/token.asmx?wsdl)|
|**IGNORE_HASH**      | Flag para utilizar durante testes. Por default é false, mas se estiver marcada como true, ela ignorará as validações de hash tanto do WS de VM quanto de BDLight|

### **Config do PARSE**

Esta parte tem todos os valores gerados dinamicamente. Não mexer a não ser que seja muito necessário.

|Constante            |Descrição                                   | 
|---------------------|--------------------------------------------|
|**PARSE_APP_ID**     | AppID do Parse
|**PARSE_MASTER_KEY** | Master key do Parse. Chave gerada dinamicamente pelo script de instalação do ambiente.|
|**PARSE_URL**        | URL com o ip da rede interna do servidor da serventia (não estou falando do loopback). Identificado no momento da instalação do script.|
|**PARSE_PORT**       | Porta do parse, por default é 1337, não há comunicação externa, apenas entre os conteiners.|
|**PARSE_ENDPOINT**   | Endpoint padrão do parse: [http://127.0.0.1/parse](http://127.0.0.1/parse)|


## **URLs do WebService** ##

Após a instalação do sistema, precisa passar as **URLs** do **WSDLs** para o Sérgio enviar pro Roberto, para ele poder gerar a `PRIV_KEY`.

Por default, as **URLs** são:

|Serviço                   |URL                                                    |
|--------------------------|-------------------------------------------------------|
|Visualização de Matrícula | [http://ip-externo-do-servidor/matricula?wsdl](http://[ip-externo-do-servidor/matricula?wsdl)|
|Bando de dados light      | [http://ip-externo-do-servidor/bdlight?wsdl](http://[ip-externo-do-servidor/bdlight?wsdl)|
