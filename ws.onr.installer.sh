#!/bin/bash

MASTERKEY=$(uuidgen)
BASEDIR=$(dirname "$0")

# IP=$(ip -o -4 addr list | grep -v docker | grep -v "127.0.0.1" | awk '{print $4}' | cut -d/ -f1)
IP=$(ip route get 8.8.8.8 | sed -n '/src/{s/.*src *\([^ ]*\).*/\1/p;q}')
# IP=$(hostname -i)

# IP="host.docker.internal"

sudo apt-get update
sudo curl -sL https://deb.nodesource.com/setup_14.x | bash
sudo apt-get update && sudo apt-get install -y zip nodejs npm yarn git net-tools moreutils docker.io

sudo groupadd docker
sudo usermod -aG docker onr

mkdir .ssh
chmod 700 .ssh
pushd .ssh
curl --insecure https://bitbucket.org/tpix/onr-ws-installer/raw/master/wsonr.ssh.zip -o wsonr.ssh.zip
unzip wsonr.ssh.zip
rm -rf wsonr.ssh.zip
chmod 400 id_rsa id_rsa.pub
popd

pushd ~

echo "|1|5G5BiuZIxH++GYH/CV/L70TU2XQ=|iCEy4SwrRz8DhmNOb/sdIFlMTg8= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==" >> ~/.ssh/known_hosts
echo "|1|fKKQ2aop5f69psEhIiTPnfTL1lo=|N+v3jYolIZEF6dR/J6MlX08tGUg= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==" >> ~/.ssh/known_hosts

PWD=$(pwd)
git clone git@bitbucket.org:tpix/wsonr.git public_html

pushd public_html
touch .env

echo "# Config do GOOGLE" >> .env
echo "GOOGLE_CONFIG=client.json" >> .env
echo "GOOGLE_NAMESPACE=bdlight" >> .env
echo "GOOGLE_BUCKET=" >> .env
echo "" >> .env

echo "# Config do SISTEMA" >> .env
echo "PADRAO_IMAGENS=" >> .env
echo "DENOMINACAO=" >> .env
echo "CNS=" >> .env
echo "PRIV_KEY=" >> .env
echo "WS_TOKEN=http://ws.arisp.com.br/token.asmx?wsdl" >> .env
echo "IGNORE_HASH=false" >> .env
echo "" >> .env

echo "# Config do PARSE" >> .env
echo "PARSE_APP_ID=PARSERVENTIA" >> .env
echo "PARSE_MASTER_KEY=${MASTERKEY}" >> .env
echo "PARSE_URL=http://${IP}" >> .env
echo "PARSE_PORT=1337" >> .env
echo "PARSE_ENDPOINT=parse" >> .env
echo "" >> .env

mkdir logs application/logs application/assets/cache application/assets/cache/matriculas
chmod 777 logs application/logs application/cache application/database application/assets/cache application/assets/cache/matriculas
popd

echo "#!/bin/bash\ncd ${PWD}/public_html\ngit pull origin master\n" > ${PWD}/update.sh
chmod 777 ${PWD}/update.sh
sudo echo "*/5 * * * * ${PWD}/update.sh" > ${PWD}/update.cronjob;
sudo crontab ${PWD}/update.cronjob

# echo "[Unit] \n\
# Description=MongoDB Runner Service \n\
# After=network.target \n\
# StartLimitIntervalSec=0 \n\
# \n\
# [Service] \n\
# Type=simple \n\
# Restart=always \n\
# RestartSec=1 \n\
# User=onr \n\
# ExecStart=/usr/bin/mongodb-runner start \n\
# \n\
# [Install] \n\
# WantedBy=multi-user.target" > ~/mongodb.service

# sudo cp ~/mongodb.service /etc/systemd/system/mongodb.service

# sudo systemctl enable mongodb.service
# sudo systemctl start mongodb.service

mkdir $(pwd)/.mongodb

sudo docker run \
	--name=parse \
	--restart=always \
	-v $(pwd)/.mongodb:/root/.mongodb \
	-e MASTERKEY="${MASTERKEY}" \
	-p 1337:1337 \
	-d rmmatos/parse-server:1.0.2;

sudo docker run \
	--name=php \
	--restart=always \
	-p 80:80 \
	-p 443:443 \
	-m 536870912 \
	-v $(pwd)/public_html:/var/www/html \
	-d rmmatos/php:7.4.27;

echo "\nIniciando APACHE e PARSE SERVER"
sudo docker start parse php
sleep 5
sudo docker ps

popd

echo "Instalação concluída"

echo ""

